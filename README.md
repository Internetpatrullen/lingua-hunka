# lingua-hunka

js applications for language study

training application for cyrillic, shows character and 3-5 alternatives for transliteration

training application for russian words, show word and 3-5 alternatives for translation to swedish or english

maybe p5?

base data structure: prolly json along the lines of numerically indexed array pointing to word/character, in turn pointing to transliterations/translations (the latter probably divided into languages, the former into alphabets)

show word/character from random numeric index, random out 2-4 transliterations/translations and random the order of alternatives before rendering

counter on success streak and percent success in session, simple immediate feedback on guess. next-button or short sleep before moving to next item?

